from asyncio import tasks
from django.shortcuts import get_object_or_404, render, redirect
from .models import TodoList
from .forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists
    }
    return render(request, 'todos/todo_list_list.html', context)




def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    tasks = todo_list.items.all()
    context = {
        "todo_list": todo_list,
        "tasks": tasks,
    }
    return render(request, 'todos/todo_list_detail.html', context)



def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()

            return redirect('todo_list_detail', id=todo_list.id)

    else:
        form = TodoListForm()

    context = {
        "form":form
    }

    return render(request, 'todos/todo_list_create.html', context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form,
        "todo_list": todo_list,
    }

    return render(request, 'todos/todo_list_update.html', context)



def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect('todo_list_list')

    context = {
        "todo_list": todo_list
    }
    return render(request, 'todos/todo_list_delete.html', context)


def todo_item_create(request, todo_list_id):
    todo_list = get_object_or_404(TodoList, id=todo_list_id)
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save(commit=False)
            todo_item.todo_list = list
            todo_item.save()
            return redirect('todo_list_detail', todo_list_id=todo_list_id)

    else:
        form = TodoItemForm()

    context = {
        "todo_list": list,
        "form" :form
    }

    return render(request, 'todos/todo_item_create.html', context)
